import java.util.ArrayList;
import java.util.Observable;

public class Model extends Observable{

    private ArrayList<String> rules;

    public Model(){
        rules = new ArrayList<>();
        fillRulesWithDefaultValues();
    }

    private void fillRulesWithDefaultValues(){
        rules.add("Wenn eine tote Zelle exakt drei lebende Nachbarn hat, wird sie wieder geboren");
        rules.add("Wenn eine lebende Zelle weniger als zwei Nachbarn hat, stirbt sie");
        rules.add("Wenn eine lebende Zelle zwei oder drei lebende Nachbarn hat, lebt sie weiter");
        rules.add("Wenn eine lebende Zelle mehr als drei Nachbarn hat, stribt sie");
    }

    public void addRule(String ruleToBeAdded){
        assert(ruleToBeAdded != null);
        rules.add(ruleToBeAdded);
        this.setChanged();
        this.notifyObservers();
    }

    public void removeRule(String ruleToBeRemoved){
        assert(ruleToBeRemoved != null);
        rules.remove(ruleToBeRemoved);
        this.setChanged();
        this.notifyObservers();
    }

    public String[] getRules(){
        String[] result = new String[rules.size()];
        for(int i = 1; i < rules.size(); ++i){
            result[i] = rules.get(i);
        }
        return result;
    }


}
