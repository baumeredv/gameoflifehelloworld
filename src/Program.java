public class Program {

    public static void main(String[] args){
        String[] regeln = {"Wenn eine tote Zelle exakt drei lebende Nachbarn hat, wird sie wieder geboren",
                "Wenn eine lebende Zelle weniger als zwei Nachbarn hat, stirbt sie",
                "Wenn eine lebende Zelle zwei oder drei lebende Nachbarn hat, lebt sie weiter",
                "Wenn eine lebende Zelle mehr als drei Nachbarn hat, stribt sie"};
        for(String regel : regeln){
            System.out.println(regel);
        }
    }

}
