import java.util.Observable;
import java.util.Observer;

public class View implements Observer{
    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Model){
            displayTheRules(((Model) arg).getRules());
        }
    }

    private void displayTheRules(String[] rules){
        System.out.println("Das sind die Regeln:");
        for(int i = 1; i < rules.length; ++i){
            System.out.println(rules[i]);
        }
    }
}
